<h2>Have a look at my [stm32f103 sdr project](https://gitlab.com/m0rph/stm32f103_sdr), it's moving along very nicely</h2>

![](stm32f411_sdr_1.png)

Working:
- 32bit adc word for both channels (16bit earch) using DMA
- 16bit PWM DMA
- 2 separate timers to trigger ADC and PWM (single timer causes pitch shift)
- CMSIS DSP
- Overclock to 150MHz
- Impressive sound quality with barebones ADC and PWM

Not working:
- Proper unwanted sideband cancellation, haven't understood the i q sum/mixer properly. ADC delay between channels is at minimum, but could affect shift.
- Single timer for ADC trigger and PWM update, causes pitch shift. This is not a problem with stm32f103 dual adc...
- All other radio functionality, displays, dds, buttons, transmit functionality etc... Will be added when the demodulation works properly.



