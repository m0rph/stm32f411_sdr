#include <libopencm3/stm32/rcc.h>
#include <libopencm3/stm32/gpio.h>
#include <libopencm3/stm32/adc.h>
#include <libopencm3/stm32/dma.h>
#include <libopencm3/stm32/timer.h>
#include <libopencm3/cm3/nvic.h>
#include <libopencm3/stm32/flash.h>
#include <coefficients.h>

#define ARM_MATH_CM4
#include <arm_math.h>

#define ADC_DMA DMA2
#define ADC_STREAM DMA_STREAM0
#define PWM_DMA DMA1
#define PWM_STREAM DMA_STREAM2

#define SAMPLE_RATE 48000 // 48828 //24414
#define PWM_SAMPLE_SIZE 8192
#define ADC_SAMPLE_SIZE PWM_SAMPLE_SIZE
volatile uint16_t buffer_adc[ADC_SAMPLE_SIZE];
volatile uint16_t buffer_pwm[PWM_SAMPLE_SIZE];
volatile uint8_t callback_state = 0;

const struct rcc_clock_scale hse_25mhz_3v3_100MHz = {
    .pllm = 12,
    .plln = 96,
    .pllp = 2,
    .pllq = 4,
    .pllr = 0,
    .pll_source = RCC_CFGR_PLLSRC_HSE_CLK,
    .hpre = RCC_CFGR_HPRE_DIV_NONE,
    .ppre1 = RCC_CFGR_PPRE_DIV_2,
    .ppre2 = RCC_CFGR_PPRE_DIV_NONE,
    .voltage_scale = PWR_SCALE1,
    .flash_config = FLASH_ACR_ICEN | FLASH_ACR_DCEN | FLASH_ACR_LATENCY_2WS,
    .ahb_frequency = 100000000,
    .apb1_frequency = 50000000,
    .apb2_frequency = 100000000,
};

const struct rcc_clock_scale hse_25mhz_3v3_150MHz = {
    .pllm = 8,
    .plln = 96,
    .pllp = 2,
    .pllq = 4,
    .pllr = 0,
    .pll_source = RCC_CFGR_PLLSRC_HSE_CLK,
    .hpre = RCC_CFGR_HPRE_DIV_NONE,
    .ppre1 = RCC_CFGR_PPRE_DIV_2,
    .ppre2 = RCC_CFGR_PPRE_DIV_NONE,
    .voltage_scale = PWR_SCALE1,
    .flash_config = FLASH_ACR_ICEN | FLASH_ACR_DCEN | FLASH_ACR_LATENCY_3WS,
    .ahb_frequency = 150000000,
    .apb1_frequency = 75000000,
    .apb2_frequency = 150000000,
};

clock_setup(void)
{
    rcc_clock_setup_pll(&hse_25mhz_3v3_100MHz);
    rcc_periph_clock_enable(RCC_GPIOA);
    rcc_periph_clock_enable(RCC_GPIOB);
    rcc_periph_clock_enable(RCC_GPIOC);
    rcc_periph_clock_enable(RCC_DMA1);
    rcc_periph_clock_enable(RCC_DMA2);
    rcc_periph_clock_enable(RCC_ADC1);
    rcc_periph_clock_enable(RCC_TIM3);
    rcc_periph_clock_enable(RCC_TIM4);
    rcc_periph_clock_enable(RCC_TIM2);
}

void adc_iq_setup(void)
{
    gpio_mode_setup(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO1 | GPIO2);
    adc_power_off(ADC1);
    adc_set_resolution(ADC1, ADC_CR1_RES_12BIT);
    adc_enable_external_trigger_regular(ADC1, ADC_CR2_EXTSEL_TIM2_TRGO, ADC_CR2_EXTEN_RISING_EDGE); // ADC_CR2_EXTSEL_SWSTART //ADC_CR2_EXTSEL_TIM3_TRGO
    adc_enable_scan_mode(ADC1);
    adc_set_sample_time_on_all_channels(ADC1, ADC_SMPR_SMP_3CYC);
    uint8_t ADC1_channel_seq[2] = {1, 2};
    adc_set_regular_sequence(ADC1, 2, ADC1_channel_seq);
    adc_enable_dma(ADC1);
    adc_set_dma_continue(ADC1);
    adc_power_on(ADC1);
}

void dma_adc_setup(void)
{
    nvic_enable_irq(NVIC_DMA2_STREAM0_IRQ);
    dma_stream_reset(ADC_DMA, ADC_STREAM);
    dma_enable_circular_mode(ADC_DMA, ADC_STREAM);
    dma_set_priority(ADC_DMA, ADC_STREAM, DMA_SxCR_PL_LOW);
    dma_enable_memory_increment_mode(ADC_DMA, ADC_STREAM);
    dma_set_peripheral_size(ADC_DMA, ADC_STREAM, DMA_SxCR_PSIZE_16BIT); // DMA_CCR_PSIZE_32BIT
    dma_set_memory_size(ADC_DMA, ADC_STREAM, DMA_SxCR_PSIZE_16BIT);
    dma_set_transfer_mode(ADC_DMA, ADC_STREAM, DMA_SxCR_DIR_PERIPHERAL_TO_MEM);
    dma_set_peripheral_address(ADC_DMA, ADC_STREAM, (uint32_t)&ADC1_DR); // ADC1_DR
    dma_set_memory_address(ADC_DMA, ADC_STREAM, (uint32_t)&buffer_adc);
    dma_set_number_of_data(ADC_DMA, ADC_STREAM, ADC_SAMPLE_SIZE);
    dma_enable_transfer_complete_interrupt(ADC_DMA, ADC_STREAM);
    dma_enable_half_transfer_interrupt(ADC_DMA, ADC_STREAM);
    dma_channel_select(ADC_DMA, ADC_STREAM, DMA_SxCR_CHSEL_0);
    dma_clear_interrupt_flags(ADC_DMA, ADC_STREAM, DMA_TCIF | DMA_DMEIF | DMA_FEIF | DMA_HTIF);
    dma_enable_stream(ADC_DMA, ADC_STREAM);
}

void dma_pwm_setup()
{
    nvic_enable_irq(NVIC_DMA1_STREAM2_IRQ);
    dma_stream_reset(PWM_DMA, PWM_STREAM);
    dma_enable_circular_mode(PWM_DMA, PWM_STREAM);
    // dma_set_priority(PWM_DMA, PWM_STREAM, DMA_SxCR_PL_LOW);
    dma_enable_memory_increment_mode(PWM_DMA, PWM_STREAM);
    dma_set_peripheral_size(PWM_DMA, PWM_STREAM, DMA_SxCR_PSIZE_16BIT);
    dma_set_memory_size(PWM_DMA, PWM_STREAM, DMA_SxCR_PSIZE_16BIT);
    dma_set_transfer_mode(PWM_DMA, PWM_STREAM, DMA_SxCR_DIR_MEM_TO_PERIPHERAL);
    dma_set_peripheral_address(PWM_DMA, PWM_STREAM, (uint32_t)&TIM4_CCR1);
    dma_set_memory_address(PWM_DMA, PWM_STREAM, (uint32_t)&buffer_pwm);
    dma_set_number_of_data(PWM_DMA, PWM_STREAM, PWM_SAMPLE_SIZE);
    dma_channel_select(PWM_DMA, PWM_STREAM, DMA_SxCR_CHSEL_5);
    dma_clear_interrupt_flags(PWM_DMA, PWM_STREAM, DMA_TCIF | DMA_DMEIF | DMA_FEIF | DMA_HTIF);
    dma_enable_stream(PWM_DMA, PWM_STREAM);
}

static void tim4_setup(void)
{
    gpio_mode_setup(GPIOB, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO6);
    gpio_set_af(GPIOB, GPIO_AF2, GPIO6);
    timer_set_prescaler(TIM4, 0);
    timer_set_period(TIM4, rcc_ahb_frequency / SAMPLE_RATE);
    timer_set_oc_mode(TIM4, TIM_OC1, TIM_OCM_PWM1);
    timer_enable_oc_output(TIM4, TIM_OC1);
    timer_enable_counter(TIM4);
}

static void tim3_setup(void)
{
    timer_set_prescaler(TIM3, 0);
    timer_set_period(TIM3, rcc_ahb_frequency / SAMPLE_RATE);
    timer_enable_irq(TIM3, TIM_DIER_UDE);
    // timer_set_master_mode(TIM3, TIM_CR2_MMS_UPDATE);
    timer_enable_counter(TIM3);
}

static void tim2_setup(void)
{
    timer_set_prescaler(TIM2, 1);
    timer_set_period(TIM2, rcc_ahb_frequency / SAMPLE_RATE);
    timer_set_master_mode(TIM2, TIM_CR2_MMS_UPDATE);
    timer_enable_counter(TIM2);
}

void dma2_stream0_isr()
{
    if (dma_get_interrupt_flag(ADC_DMA, ADC_STREAM, DMA_HTIF))
    {
        callback_state = 1;
    }
    else if (dma_get_interrupt_flag(ADC_DMA, ADC_STREAM, DMA_TCIF))
    {
        callback_state = 2;
    }
    dma_clear_interrupt_flags(ADC_DMA, ADC_STREAM, DMA_TCIF | DMA_DMEIF | DMA_FEIF | DMA_HTIF | DMA_TEIF);
}

void dma1_stream2_isr()
{
    dma_clear_interrupt_flags(PWM_DMA, PWM_STREAM, DMA_TCIF | DMA_DMEIF | DMA_FEIF | DMA_HTIF | DMA_TEIF);
}

int main(void)
{
    clock_setup();
    dma_pwm_setup();
    dma_adc_setup();
    tim4_setup();
    tim3_setup();
    tim2_setup();
    adc_iq_setup();

    volatile int dma_pointer;
    const uint16_t blockLen = 1;
    float state_right[NO_HILBERT_COEFFS + blockLen - 1]; // coeff_count + instanace_sample_count -1
    float state_left[NO_HILBERT_COEFFS + blockLen - 1];
    arm_fir_instance_f32 fir_right, fir_left;
    arm_fir_init_f32(&fir_right, NO_HILBERT_COEFFS, Hilbert_Plus_45_Coeffs, state_right, blockLen);
    arm_fir_init_f32(&fir_left, NO_HILBERT_COEFFS, Hilbert_Minus_45_Coeffs, state_left, blockLen);

    while (1)
    {
        if (callback_state != 0)
        {
            if (callback_state == 1)
            {
                dma_pointer = 0;
            }

            else if (callback_state == 2)
            {
                dma_pointer = ADC_SAMPLE_SIZE / 2;
            }

            for (size_t i = dma_pointer; i < ADC_SAMPLE_SIZE; i += 2)
            {
                float buffer_adc_right = buffer_adc[i];
                float buffer_adc_left = buffer_adc[i + 1];
                float buffer_pwm_right;
                float buffer_pwm_left;
                arm_fir_f32(&fir_right, &buffer_adc_right, &buffer_pwm_right, blockLen);
                arm_fir_f32(&fir_left, &buffer_adc_left, &buffer_pwm_left, blockLen);
                buffer_pwm[i] = (buffer_pwm_left + 1 * buffer_pwm_right) * 1;
            }
            callback_state = 0;
        }
        // for (size_t i = 0; i < ADC_SAMPLE_SIZE; i++)
        // {
        //     buffer_pwm[i] = buffer_adc[i];
        // }
    }
    return 0;
}